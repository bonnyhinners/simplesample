@isTest
public class SimpleTest {
    @isTest
    public static void verifySomethingSimple() {
        List<ApexClass> classList = [SELECT id, name from ApexClass where name='SimpleTest'];
        System.assert(classList.size()==1, 'Test class name has changed.');
    }
}
